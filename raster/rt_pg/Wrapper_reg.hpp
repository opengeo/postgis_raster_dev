#ifndef WRAPPER_REG_H
#define WRAPPER_REG_H

#ifdef __cplusplus
extern "C" {
#endif

  u_char*  ST_register(int height,int width,char type, void* pband,void* To_pband,int rowbytes);

#ifdef __cplusplus
}
#endif
#endif
